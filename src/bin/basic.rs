extern crate png;
extern crate rand;

use std::path::Path;
use std::fs::File;
use std::io::BufWriter;
use std::f32::consts::PI;
use std::f32;

use png::HasParameters;
use rand::Rng;

fn circleSDF(x: f32, y: f32, cx: f32, cy: f32, r: f32) -> f32 {
    let ux = x - cx;
    let uy = y - cy;
    (ux * ux + uy * uy).sqrt() - r
}

fn trace(ox: f32, oy: f32, dx: f32, dy: f32) -> f32 {
    let MAX_STEP = 10;
    let MAX_DISTANCE = 2.0;
    let EPSILON = 1e-6;
    let mut t = 0.0;
    let mut i = 0;
    loop {
        if i < MAX_STEP && t < MAX_DISTANCE {
            let sd = circleSDF(ox + dx * t, oy + dy * t, 0.5, 0.5, 0.1);
            if sd < EPSILON {
                return 2.0;
            }
            t += sd;
        } else {
            break;
        }
        i += 1;
    }
    return 0.0;
}

fn sample(x: f32, y: f32) -> f32 {
    let mut rng = rand::thread_rng();
    let N: u8 = 128;
    let mut sum = 0.0;
    for i in 0..N {
        // let a = 2.0 * PI * rng.gen::<f32>();                         // 均匀采样
        // let a = 2.0 * PI * (i as f32) / (N as f32);                  // 分层采样
        let a = 2.0 * PI * (i as f32 + rng.gen::<f32>()) / (N as f32);  // 抖动采样
        sum += trace(x, y, a.cos(), a.sin());
    }
    sum / (N as f32)
}

fn main() {
    let width = 512;
    let height = 512;

    let path = Path::new("images/basic.png");
    let file = File::create(path).unwrap();
    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, width, height);   // Width is 2 pixels and height is 1.
    encoder.set(png::ColorType::RGBA).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    let capacity = (width * height * 4) as usize;
    let mut data: Vec<u8> =  Vec::with_capacity(capacity);;

    for y in 0..height {
        for x in 0..width {
            let x_ = x as f32 / width as f32;
            let y_ = y as f32 / height as f32;
            let p = sample(x_, y_).min(1.0) * 255.0;
            let p = p as u8;
            data.extend(vec![p, p, p, 255]);
        }
    }
    writer.write_image_data(&data[0..]).unwrap();    // Save
}
